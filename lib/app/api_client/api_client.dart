import "package:dio/dio.dart";

class NewsApiClient {
  static Dio dio = Dio();

  static Dio make() {
    if (dio.interceptors.isEmpty) {
      dio.interceptors.add(
        InterceptorsWrapper(
          onRequest: (
            RequestOptions options,
            RequestInterceptorHandler handler,
          ) async {
            var customHeaders = <String, dynamic>{};

            options.headers.addAll(customHeaders);
            return handler.next(options);
          },
        ),
      );
    }
    return dio;
  }
}
