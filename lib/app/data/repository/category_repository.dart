// ignore_for_file: type_annotate_public_apis, empty_catches

import "package:dio/dio.dart";

import "../../common/strings.dart";

class CategoryRepository {
  final Dio? httpClient;

  CategoryRepository({required this.httpClient});

  Future<Map<String, dynamic>?> getCategoryNews() async {
    try {
      var response = await httpClient!.get(
          "https://newsapi.org/v2/top-headlines/sources?apiKey=${Strings.apiKey}");
      var body = response.data;

      return body;
    } on DioError {}
    return null;
  }
}
