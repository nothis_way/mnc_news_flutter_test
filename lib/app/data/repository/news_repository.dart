// ignore_for_file: type_annotate_public_apis, empty_catches

import "package:dio/dio.dart";

import "../../common/strings.dart";

class NewsRepositort {
  final Dio? httpClient;

  NewsRepositort({required this.httpClient});

  Future<Map<String, dynamic>?> getNewsSource(source) async {
    print("source getNewsSource : $source");
    try {
      var response = await httpClient!.get(
        "https://newsapi.org/v2/top-headlines?sources=$source&apiKey=${Strings.apiKey}",
      );
      var body = response.data;

      return body;
    } on DioError {}
    return null;
  }
}
