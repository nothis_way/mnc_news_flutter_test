import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:news_mnc/app/modules/webview_article/bindings/webview_article_binding.dart';
import 'package:news_mnc/app/modules/webview_article/views/webview_article_view.dart';

import '../../../common/strings.dart';
import '../controllers/news_controller.dart';

class NewsView extends GetView<NewsController> {
  const NewsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(controller.sourceName.value),
          backgroundColor: Colors.purple,
          centerTitle: false,
          actions: [
            IconButton(onPressed: () {}, icon: const Icon(Icons.search))
          ],
        ),
        body: Obx(() => controller.listNews.isEmpty
            ? const Center(
                child: Text("Tidak ada artikel"),
              )
            : ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                itemCount: controller.listNews.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      Get.to(
                        () => const WebviewArticleView(),
                        binding: WebviewArticleBinding(),
                        arguments: {
                          "title": controller.listNews[index].title,
                          "url": controller.listNews[index].url,
                        },
                      );
                    },
                    child: Container(
                      padding: const EdgeInsets.all(20.0),
                      decoration: const BoxDecoration(
                          border: Border(
                              bottom: const BorderSide(
                                  color: Colors.grey, width: 0.3))),
                      child: Row(
                        children: [
                          Container(
                            height: 100.0,
                            width: 100,
                            decoration: BoxDecoration(
                              //let's add the height
                              image: DecorationImage(
                                  image: NetworkImage(
                                      controller.listNews[index].urlToImage ??
                                          Strings.imageUrlPlaceHolder),
                                  fit: BoxFit.cover),
                            ),
                          ),
                          const SizedBox(width: 20,),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  controller.listNews[index].title ?? "-",
                                  maxLines: 2,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16.0,
                                      overflow: TextOverflow.ellipsis
                                  ),
                                ),
                                const SizedBox(height: 10,),
                                Text(
                                  controller.listNews[index].description ?? "-",
                                  maxLines: 3,
                                  style: const TextStyle(
                                      fontSize: 16.0,
                                      overflow: TextOverflow.ellipsis
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                },
              )));
  }
}
