import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:news_mnc/app/data/model/news_model.dart';
import 'package:news_mnc/app/data/repository/news_repository.dart';

class NewsController extends GetxController {
  //TODO: Implement NewsController

  final listNews = <NewsModel>[].obs;

  NewsRepositort newsRepositort;
  NewsController({required this.newsRepositort});
  final sourceNews = ''.obs;
  final sourceName = ''.obs;
  @override
  void onInit() async {
    super.onInit();
    sourceNews.value = Get.arguments["source"];
    sourceName.value = Get.arguments["sourceName"];
    await getSourceNews();
  }

  @override
  void onReady() async {
    super.onReady();

    sourceNews.value = Get.arguments["source"];

    print(sourceNews.value);

    await getSourceNews();
  }

  @override
  void onClose() {
    super.onClose();
  }

  Future<void> getSourceNews() async {
    try {
      Get.dialog(
        LoadingAnimationWidget.hexagonDots(
          color: Colors.red,
          size: 25,
        ),
      );
      var getNewsSource = await newsRepositort.getNewsSource(sourceNews.value);
      if (getNewsSource != null) {
        Get.close(1);
        for (var item in getNewsSource["articles"]) {
          var temp = NewsModel.fromJson(item);
          listNews.add(temp);
        }
      }
    } catch (e) {
      print(e.toString());
    }
  }
}
