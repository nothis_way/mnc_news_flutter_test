import 'package:get/get.dart';
import 'package:news_mnc/app/api_client/api_client.dart';
import 'package:news_mnc/app/data/repository/news_repository.dart';

import '../controllers/news_controller.dart';

class NewsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<NewsController>(
      () => NewsController(
          newsRepositort: NewsRepositort(httpClient: NewsApiClient.make())),
    );
  }
}
