import 'package:get/get.dart';
import 'package:news_mnc/app/api_client/api_client.dart';
import 'package:news_mnc/app/data/repository/category_repository.dart';

import '../controllers/home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(
      () => HomeController(
        categoryRepository: CategoryRepository(
          httpClient: NewsApiClient.make(),
        ),
      ),
    );
  }
}
