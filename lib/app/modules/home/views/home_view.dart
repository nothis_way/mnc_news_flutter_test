import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:news_mnc/app/modules/sources/bindings/source_binding.dart';
import 'package:news_mnc/app/modules/sources/views/source_view.dart';

import '../../../common/strings.dart';
import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.purple,
            title: const Text('Categories'),
            centerTitle: false,
          ),
          body: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: GridView.builder(
                      shrinkWrap: true,
                      itemCount: controller.listCategory.length,
                      itemBuilder: (context, index) {
                        return Obx(() => InkWell(
                              onTap: () {
                                Get.to(
                                      () => const SourceView(),
                                  binding: SourceBinding(),
                                  arguments: {
                                    "source": controller.listCategory[index],
                                    "listCategoryTemp": controller.listCategoryTemp.toJson()
                                  },
                                );
                              },
                              onHover: (value) {
                                controller.isHover.value = value;
                              },
                              onFocusChange: (value) {
                                controller.isHover.value = value;
                              },
                              child: Container(
                                  height: 400,
                                  margin: EdgeInsets.only(left: 5),
                                  padding: EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Colors.grey.withOpacity(0.5),
                                    image: const DecorationImage(
                                      image: NetworkImage(
                                        Strings.categoryNewsImageUrl,
                                      ),
                                      fit: BoxFit.fitWidth
                                    )
                                  ),
                                  child: Text(controller.listCategory[index], style: TextStyle(fontWeight: FontWeight.bold),)),
                            ));
                      },
                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 16,
                      mainAxisSpacing: 16,
                      childAspectRatio: 2.0
                    ),
                    ),
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
        ));
  }
}
