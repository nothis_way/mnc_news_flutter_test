import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:get/get.dart';
import 'package:news_mnc/app/data/model/category_model.dart';
import 'package:news_mnc/app/data/repository/category_repository.dart';

class HomeController extends GetxController {
  //TODO: Implement HomeController
  final listCategoryTemp = <CategoryModel>[].obs;
  final listCategory = [].obs;

  final isHover = false.obs;

  CategoryRepository categoryRepository;
  HomeController({required this.categoryRepository});

  @override
  void onInit() async {
    super.onInit();
    await getCategoryNews();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  Future<void> getCategoryNews() async {
    try {
      var getCategoryNews = await categoryRepository.getCategoryNews();

      if (getCategoryNews != null) {

        for (var item in getCategoryNews["sources"]) {
          var category = CategoryModel.fromJson(item);
          listCategoryTemp.add(category);
        }

        var newMap = groupBy(listCategoryTemp, (CategoryModel obj) => obj.category);

        for (var temp2 in newMap.keys.toList()) {
          listCategory.add(temp2);
        }
      }
    } catch (e) {
      print(e.toString());
    }
  }
}
