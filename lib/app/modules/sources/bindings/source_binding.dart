import 'package:get/get.dart';
import 'package:news_mnc/app/modules/sources/controllers/source_controller.dart';

class SourceBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SourceController>(
          () => SourceController(),
    );
  }
}
