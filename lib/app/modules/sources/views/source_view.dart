import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:news_mnc/app/modules/sources/controllers/source_controller.dart';

import '../../news/bindings/news_binding.dart';
import '../../news/views/news_view.dart';

class SourceView extends GetView<SourceController> {
  const SourceView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(controller.sourceNews.value),
        backgroundColor: Colors.purple,
      ),
      body: ListView.builder(
        scrollDirection: Axis.vertical,
        // physics: const NeverScrollableScrollPhysics(),
        itemCount: controller.listSource.length,
        itemBuilder: (context, index) {
          return Obx(() => InkWell(
                onTap: () {
                  Get.to(
                    () => const NewsView(),
                    binding: NewsBinding(),
                    arguments: {
                      "source":
                          controller.listSource[index].id.toString(),
                      "sourceName": controller.listSource[index].name.toString()
                    },
                  );
                },
                child: Container(
                  margin: const EdgeInsets.symmetric(vertical: 5),
                  padding: const EdgeInsets.all(10),
                  decoration: const BoxDecoration(
                    border: Border(bottom: BorderSide(color: Colors.grey, width: 0.2))
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(controller.listSource[index].name!, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),),
                      SizedBox(
                        height: 10,
                      ),
                      Text(controller.listSource[index].description!),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ));
        },
      ),
    );
  }
}
