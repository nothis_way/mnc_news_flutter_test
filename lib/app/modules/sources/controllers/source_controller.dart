import 'dart:convert';

import 'package:get/get.dart';

import '../../../data/model/category_model.dart';

class SourceController extends GetxController {

  final listCategoryTemp = <CategoryModel>[].obs;
  final listSource = <CategoryModel>[].obs;

  final sourceNews = ''.obs;


  @override
  void onInit() async {
    super.onInit();
    listCategoryTemp.value = Get.arguments['listCategoryTemp'];
    sourceNews.value = Get.arguments["source"];

    await getSourceNews(sourceNews.value);
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }




  Future<void> getSourceNews(String text) async {
    print(jsonEncode(listCategoryTemp));
    listSource.clear();
    if (text.isEmpty) {
      return;
    }
    for (var item in listCategoryTemp) {
      if (item.category!
          .contains(RegExp("" + text + "", caseSensitive: false))) {
        listSource.add(item);
        print("list source length: ${listSource.length}");
      }
    }
  }
}