import 'package:get/get.dart';

import '../controllers/webview_article_controller.dart';

class WebviewArticleBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<WebviewArticleController>(
      () => WebviewArticleController(),
    );
  }
}
