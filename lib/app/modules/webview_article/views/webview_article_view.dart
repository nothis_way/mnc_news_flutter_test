import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/webview_article_controller.dart';
import "package:webview_flutter/webview_flutter.dart";

class WebviewArticleView extends GetView<WebviewArticleController> {
  const WebviewArticleView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
          body: WebView(
            initialUrl: controller.url.value,
            onWebViewCreated: (_controller) {
              controller.webViewController = _controller;
            },
          ),
        ));
  }
}
