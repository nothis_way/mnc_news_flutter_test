import 'package:get/get.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebviewArticleController extends GetxController {
  //TODO: Implement WebviewArticleController

  final url = "".obs;
  final title = "".obs;

  WebViewController? webViewController;
  @override
  void onInit() {
    super.onInit();
    title.value = Get.arguments["title"] ?? "";
    url.value = Get.arguments["url"] ?? "";
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
