// ignore_for_file: constant_identifier_names

part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const NEWS = _Paths.NEWS;
  static const SOURCE = _Paths.SOURCE;
  static const WEBVIEW_ARTICLE = _Paths.WEBVIEW_ARTICLE;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const SOURCE = '/source';
  static const NEWS = '/news';
  static const WEBVIEW_ARTICLE = '/webview-article';
}
