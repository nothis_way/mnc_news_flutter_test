import 'package:get/get.dart';
import 'package:news_mnc/app/modules/sources/bindings/source_binding.dart';
import 'package:news_mnc/app/modules/sources/views/source_view.dart';

import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/news/bindings/news_binding.dart';
import '../modules/news/views/news_view.dart';
import '../modules/webview_article/bindings/webview_article_binding.dart';
import '../modules/webview_article/views/webview_article_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => const HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.NEWS,
      page: () => const NewsView(),
      binding: NewsBinding(),
    ),
    GetPage(
      name: _Paths.WEBVIEW_ARTICLE,
      page: () => const WebviewArticleView(),
      binding: WebviewArticleBinding(),
    ),
    GetPage(
      name: _Paths.SOURCE,
      page: () => const SourceView(),
      binding: SourceBinding(),
    ),
  ];
}
