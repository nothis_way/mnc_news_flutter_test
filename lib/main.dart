// ignore_for_file: await_only_futures

import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:news_mnc/app/api_client/api_client.dart';
import 'package:news_mnc/app/data/repository/category_repository.dart';
import 'package:news_mnc/app/modules/home/controllers/home_controller.dart';

import 'app/routes/app_pages.dart';

void main() {
  runApp(
    GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Application",
      onInit: () {
        Get.put(
          HomeController(
            categoryRepository: CategoryRepository(
              httpClient: NewsApiClient.make(),
            ),
          ),
        );
      },
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
    ),
  );
}
